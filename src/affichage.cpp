#include <iostream>
#include <iomanip>
#include <limits>
#include <cstring>
#include <libpq-fe.h>
#include "affichage.h"
#include "calcul.h"

void afficher_etat_connexion(PGconn *connexion)
{
  unsigned short int nb_caracteres;
  std::cout << "La connexion au serveur de base de données '" << PQhost(connexion)  << "' a été établie avec les paramètres suivants :" << std::endl;
  std::cout << " * utilisateur : " << PQuser(connexion) << std::endl;
  std::cout << " * mot de passe : ";
  nb_caracteres = sizeof PQpass(connexion);

  for(unsigned int i = 0; i < nb_caracteres; ++i)
  {
    std::cout << '*';
  }

  std::cout << " (<-- autant de '*' que la longueur du mot de passe)" << std::endl;
  std::cout << " * base de données : " << PQdb(connexion) << std::endl;
  std::cout << " * port TCP : " << PQport(connexion) << std::endl;
  std::cout << std::boolalpha << " * chiffrement SSL : " << (bool)(PQgetssl(connexion) != 0) << std::endl;
  std::cout << " * encodage : " << pg_encoding_to_char(PQclientEncoding(connexion)) << std::endl;
  std::cout << " * version du protocole : " << PQprotocolVersion(connexion) << std::endl;
  std::cout << " * version du serveur : " << PQserverVersion(connexion) << std::endl;
  std::cout << " * version de la bibliothèque 'libpq' du client : " << PQlibVersion() << std::endl;
}

void afficher_ligne_separatrice(const char symbole, unsigned int longueur)
{
  for(unsigned int numero_caractere = 0; numero_caractere < longueur; ++numero_caractere)
  {
    std::cout << symbole;
  }

  std::cout << std::endl;
}

void afficher_en_tete(PGresult *donnees, unsigned int largeur_champ, const char separateur_champs, const char marge_separateur)
{
  for(unsigned int compteur_champ = 0; (int)compteur_champ < PQnfields(donnees); ++compteur_champ)
  {
    std::cout << separateur_champs
              << marge_separateur
              << std::setw(largeur_champ)
              << PQfname(donnees, compteur_champ)
              << marge_separateur;
  }

  std::cout << separateur_champs << std::endl;
}

void afficher_tuples(PGresult *donnees, unsigned int largeur_champ, const char separateur_champs, const char marge_separateur)
{
  char *valeur_champ;

  for(unsigned int compteur_tuple = 0; (int)compteur_tuple < PQntuples(donnees); ++compteur_tuple)
  {
    for(unsigned int compteur_champ = 0; (int)compteur_champ < PQnfields(donnees); ++compteur_champ)
    {
      char *tmp;
      tmp = (char *)std::malloc(largeur_champ * sizeof(char) + 1);
      valeur_champ = PQgetvalue(donnees, compteur_tuple, compteur_champ);

      if(PQgetlength(donnees, compteur_tuple, compteur_champ) > (int)largeur_champ)
      {
        valeur_champ = std::strncpy(tmp, valeur_champ, largeur_champ);
        valeur_champ[largeur_champ] = 0;

        for(unsigned int i = 1; i <= 3; valeur_champ[largeur_champ - i++] = '.');
      }

      std::cout << separateur_champs
                << marge_separateur
                << std::setw(largeur_champ)
                << valeur_champ
                << marge_separateur;
      std::free(tmp);
    }

    std::cout << separateur_champs << std::endl;
  }
}

void afficher_tableau(PGresult *donnees, const char separateur_ligne, const char separateur_champs, const char marge_separateur, const char alignement)
{
  unsigned int largeur_ligne = largeur_maximum_tableau(donnees, separateur_champs, marge_separateur);
  unsigned int largeur_champ = largeur_maximum_nom_champ(donnees);

  if(alignement == 'l')
  {
    std::cout << std::left;
  }
  else if(alignement == 'r')
  {
    std::cout << std::right;
  }
  else
  {
    std::cout << std::internal;
  }

  afficher_ligne_separatrice(separateur_ligne, largeur_ligne);
  afficher_en_tete(donnees, largeur_champ, separateur_champs, marge_separateur);
  afficher_ligne_separatrice(separateur_ligne, largeur_ligne);
  afficher_tuples(donnees, largeur_champ, separateur_champs, marge_separateur);
  afficher_ligne_separatrice(separateur_ligne, largeur_ligne);
  std::cout << std::endl;
  std::cout << "L'exécution de la requête SQL ";
  std::cout << "a retourné " << PQntuples(donnees) << " enregistrement" << (PQntuples(donnees) > 1 ? 's' : '\0') << "." << std::endl;
}

