#include <iostream>
#include <limits>
#include <cstring>
#include <libpq-fe.h>
#include "calcul.h"

unsigned int largeur_maximum_tableau(PGresult *donnees, const char separateur_champs, const char marge_separateur)
{
  unsigned int taille_separateur_champs = 1;
  unsigned int taille_marge_separateur = 1;

  if(separateur_champs == '\0')
  {
    taille_separateur_champs = 0;
  }

  if(marge_separateur == '\0')
  {
    taille_marge_separateur = 0;
  }

  return PQnfields(donnees) * (taille_separateur_champs + 2 * taille_marge_separateur + largeur_maximum_nom_champ((donnees))) + taille_separateur_champs;
}

unsigned int largeur_maximum_nom_champ(PGresult *donnees)
{
  unsigned int taille_champ = 0;
  unsigned int champ_maximum = std::numeric_limits<unsigned int>::min();
  char *nom_de_champ;

  // Recherche de la taille du nom de champ le plus large
  for(unsigned int compteur_champ = 0; (int)compteur_champ < PQnfields(donnees); ++compteur_champ)
  {
    nom_de_champ = PQfname(donnees, compteur_champ);
    taille_champ = std::strlen(nom_de_champ);

    if(champ_maximum < taille_champ)
    {
      champ_maximum = taille_champ;
    }
  }

  return champ_maximum;
}

