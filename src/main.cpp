//https://framagit.org/sio/si6/si6-cpp-libpq-postgresql/blob/prog/factorisation/data/main.cpp
#include <iostream>
#include <iomanip>
#include <limits>
#include <cstring>
#include <libpq-fe.h>
#include "affichage.h"

int main()
{
  const char *connexion_infos;
  PGconn *connexion;
  connexion_infos = "host='postgresql.bts-malraux72.net' port=5432 dbname='l.mauger' user='l.mauger' connect_timeout=5";

  if(PQping(connexion_infos) == PQPING_OK)
  {
    // Établissement de la connexion avec les paramètres
    // fournis dans la chaîne de caractères 'connexion_infos'
    connexion = PQconnectdb(connexion_infos);

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      // Affichage des informations de connexion
      afficher_etat_connexion(connexion);
      // Récupération des données
      const char *requete_sql = "SELECT si6.\"Animal\".id, si6.\"Animal\".nom as \"nom de l\'animal\", sexe, date_naissance AS \"date de naissance\", commentaires, si6.\"Race\".nom AS race, description FROM si6.\"Animal\" JOIN si6.\"Race\" on si6.\"Animal\".race_id = si6.\"Race\".id WHERE si6.\"Animal\".sexe = 'Femelle' AND si6.\"Race\".nom = 'Singapura';";
      PGresult *resultats = PQexec(connexion, requete_sql);
      ExecStatusType etat_execution = PQresultStatus(resultats);

      // Vérification de l'état des données reçues
      if(etat_execution == PGRES_TUPLES_OK)
      {
        // Exploitation des résultats
        afficher_tableau(resultats);
      }
      else if(etat_execution == PGRES_EMPTY_QUERY)
      {
        std::cerr << "La chaîne envoyée au serveur était vide." << std::endl;
      }
      else if(etat_execution == PGRES_COMMAND_OK)
      {
        std::cerr << "La requête SQL n'a renvoyé aucune donnée." << std::endl;
      }
      else if(etat_execution == PGRES_COPY_OUT)
      {
        std::cerr << "Début de l'envoi (à partir du serveur) d'un flux de données." << std::endl;
      }
      else if(etat_execution == PGRES_COPY_IN)
      {
        std::cerr << "Début de la réception (sur le serveur) d'un flux de données." << std::endl;
      }
      else if(etat_execution == PGRES_BAD_RESPONSE)
      {
        std::cerr << "La réponse du serveur n'a pas été comprise." << std::endl;
      }
      else if(etat_execution == PGRES_NONFATAL_ERROR)
      {
        std::cerr << "Une erreur non fatale (une note ou un avertissement) est survenue." << std::endl;
      }
      else if(etat_execution == PGRES_FATAL_ERROR)
      {
        std::cerr << "Une erreur fatale est survenue." << std::endl;
      }
      else if(etat_execution == PGRES_COPY_BOTH)
      {
        std::cerr << "Lancement du transfert de données Copy In/Out (vers et à partir du serveur)." << std::endl;
      }
      else if(etat_execution == PGRES_SINGLE_TUPLE)
      {
        std::cerr << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante." << std::endl;
      }

      PQclear(resultats);
    }
    else
    {
      std::cerr << "Malheureusement la connexion n'a pas pu être établie" << std::endl;
    }

    PQfinish(connexion);
  }
  else
  {
    std::cerr << "Malheureusement le serveur n'est pas joignable. Vérifier la connectivité" << std::endl;
  }

  return 0;
}
