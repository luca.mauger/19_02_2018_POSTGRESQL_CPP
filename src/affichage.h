void afficher_etat_connexion(PGconn *);
void afficher_ligne_separatrice(const char, unsigned int);
void afficher_en_tete(PGresult *, unsigned int = 0, const char = '|', const char = ' ');
void afficher_tuples(PGresult *, unsigned int = 0, const char = '|', const char = ' ');
void afficher_tableau(PGresult *, const char = '-', const char = '|', const char = ' ', const char = 'l');

