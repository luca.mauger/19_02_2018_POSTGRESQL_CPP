CXX = g++
CFLAGS = $(shell pkg-config --cflags libpq)
LDFLAGS = $(shell pkg-config --libs libpq) 

bin/programme: obj/main.o obj/calcul.o obj/affichage.o 
	$(CXX) $(LDFLAGS)  -o $@ $^

obj/calcul.o: src/calcul.cpp
	$(CXX) $(CFLAGS) -c -o $@ $^

obj/affichage.o: src/affichage.cpp
	$(CXX) $(CFLAGS) -c -o $@ $^

bin/main: obj/main.o
	$(CXX) $(LDFLAGS)  -o $@ $^

obj/main.o: src/main.cpp
	$(CXX) $(CFLAGS) -c -o $@ $^

clean:
	-rm bin/* obj/*.o

repertoire:
	mkdir bin obj src
	touch bin/.gitkeep obj/.gitkeep

